﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Wave Config")]
public class WaveConfig : ScriptableObject
{
    [SerializeField] GameObject path;
    [SerializeField] GameObject enemy;
    [SerializeField] int enemiesNumber;
    [SerializeField] float enemySpeed;
    [SerializeField] float spawnRate;

    public List<Transform> GetWaypoints() {
        List<Transform> waypoints = new List<Transform>();

        foreach (Transform child in path.transform)
        {
            waypoints.Add(child);
        }

        return waypoints;
    }
    public GameObject GetEnemy() { return enemy; }
    public int GetEnemiesNumber() { return enemiesNumber; }
    public float GetEnemySpeed() { return enemySpeed; }
    public float GetSpawnRate() { return spawnRate; }
}
