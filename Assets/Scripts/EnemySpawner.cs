﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] bool endlessMode;
    [SerializeField] List<WaveConfig> waves;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (endlessMode);
    }

    IEnumerator SpawnAllWaves()
    {
        for (int waveIndex = 0; waveIndex < waves.Count; waveIndex++)
        {
            yield return StartCoroutine(SpawnEnemies(waves[waveIndex]));
        }
    }

    IEnumerator SpawnEnemies(WaveConfig waveConfig)
    {
        for (int i = 0; i < waveConfig.GetEnemiesNumber(); i++)
        {
            GameObject newEnemy = Instantiate(waveConfig.GetEnemy());
            EnemyMovement enemyMovement = newEnemy.GetComponent<EnemyMovement>();
            enemyMovement.SetWaypoints(waveConfig.GetWaypoints());
            enemyMovement.SetSpeed(waveConfig.GetEnemySpeed());

            yield return new WaitForSeconds(waveConfig.GetSpawnRate());
        }
    }
}
