﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float padding;
    [SerializeField] float fireRate = 1f;

    [SerializeField] GameObject bullet;

    Vector3 xyMin;
    Vector3 xyMax;

    float nextFire;

    // Start is called before the first frame update
    void Start()
    {
        Camera gameCamera = Camera.main;
        xyMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0));
        xyMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Shoot();
    }

    private void Move()
    {
        Vector2 currentPos = transform.position;

        float inputX = Input.GetAxis("Horizontal");
        float deltaX = inputX * speed * Time.deltaTime;
        float inputY = Input.GetAxis("Vertical");
        float deltaY = inputY * speed * Time.deltaTime;

        transform.position = new Vector2(currentPos.x + deltaX, currentPos.y + deltaY);

        float newX = Mathf.Clamp(transform.position.x, xyMin.x + padding, xyMax.x - padding);
        float newY = Mathf.Clamp(transform.position.y, xyMin.y + padding, xyMax.y - padding);
        transform.position = new Vector2(newX, newY);
    }

    private void Shoot()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextFire)
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
            nextFire = Time.time + fireRate;
        }
    }

}
